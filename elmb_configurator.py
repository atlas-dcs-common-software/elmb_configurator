#!/usr/bin/env python

import canopen
import argparse
import sys

class Error(Exception):
    pass

if __name__ == "__main__":
    
    parser = argparse.ArgumentParser(
        description= '''Script to configure ELMBs'''
    )
    parser.add_argument('--saveEeprom', metavar='\b', dest='EEPROM', type=int, default=0, help='Save configuration to EEPROM. 0 = No (Default) / 1 = Yes')
    parser.add_argument('--canBusPort', metavar='\b', dest='canBusPort', type=int, help='CanBus port number')
    parser.add_argument('--nodeId', metavar='\b', dest='nodeId', type=int, help='Node Id (Decimal)')
    parser.add_argument('--polarity', metavar='\b',  dest='polarity', type=int, help='0 = Bipolar / 1 = Unipolar')
    parser.add_argument('--adcRate', metavar='\b',  dest='adcRate', type=float, help='ADC Conversion Rate (Hz)')
    parser.add_argument('--adcRange', metavar='\b',  dest='adcRange', type=float, help='ADC Voltage Range (V)')
    parser.add_argument('--adcChannels', metavar='\b',  dest='adcChannels', type=int, help='Max number of ADC channels')
    parser.add_argument('--printConfig',  dest='printConfig', action='store_true', help='Print current configuration (No arg)')
    
    args=parser.parse_args()
    
    try:
        if not args.canBusPort:
            raise Error
    except Error:
        print("ERROR: No CanBus port number provided")
        
    try:
        if not args.nodeId:
            raise Error
    except Error:
        print("ERROR: No ELMB NodeId provided")
    
    od = canopen.ObjectDictionary()
    network = canopen.Network()
    network.connect(channel='can' + str(args.canBusPort), bustype='socketcan', bitrate=125000)
    
    network.check()
    
    node = canopen.BaseNode402(args.nodeId, od)
    network.add_node(node)
    sdo = node.add_sdo(0x600 + args.nodeId, 0x580 + args.nodeId)
    
    if args.printConfig:
        #Read and print current configuration
        print("---------------------------------")
        print("Current Elmb ADC configuration:")
        print("---------------------------------")
        
        #Input channels:
        input_channels = node.sdo.upload(0x2100, 1)
        output = int.from_bytes(input_channels, byteorder=sys.byteorder)
        print("Number of input channels:",output)
        
        #Input voltage range:
        input_voltage = node.sdo.upload(0x2100, 3)
        output = int.from_bytes(input_voltage, byteorder=sys.byteorder)
        if(output == 0) : print("Input volgate: 100mV")
        elif(output == 1) : print("Input voltage: 55mV")
        elif(output == 2) : print("Input voltage: 25mV")
        elif(output == 3) : print("Input voltage: 1V")
        elif(output == 4) : print("Input voltage: 5V")
        elif(output == 5) : print("Input voltage: 2.5V")
        
        #Measurement mode
        polarity = node.sdo.upload(0x2100, 4)
        output = int.from_bytes(polarity, byteorder=sys.byteorder)
        if(output) : print("Polarity: Unipolar")
        else : print("Polarity: Bipolar")
        
        #Conversion rate
        conversion_rate = node.sdo.upload(0x2100, 2)
        output = int.from_bytes(conversion_rate, byteorder=sys.byteorder)
        if(output == 0) : print("Conversion rate: 15Hz")
        elif(output == 1) : print("Conversion rate: 30Hz")
        elif(output == 2) : print("Conversion rate: 61.6Hz")
        elif(output == 3) : print("Conversion rate: 84.5Hz")
        elif(output == 4) : print("Conversion rate: 101.1Hz")
        elif(output == 5) : print("Conversion rate: 1.88Hz")
        elif(output == 6) : print("Conversion rate: 3.76Hz")
        elif(output == 7) : print("Conversion rate: 7.51Hz")
        
        print("---------------------------------")
        
    else:
        
        #Go into PREOP mode
        node.nmt.state = 'PRE-OPERATIONAL'
        
        #Set polarity
        if args.polarity is not None:
            node.sdo.download(0x2100, 4, args.polarity.to_bytes(1, 'big'))
            if(args.polarity == 0) : print("Polarity set to Bipolar")
            else : print("Polarity set to Unipolar")
        
        
        #adcRate WORKS
        if args.adcRate is not None:
            if(args.adcRate == 15) : node.sdo.download(0x2100, 2, b'\x00')
            elif(args.adcRate == 30) : node.sdo.download(0x2100, 2, b'\x01')
            elif(args.adcRate == 61.6) : node.sdo.download(0x2100, 2, b'\x02')
            elif(args.adcRate == 84.5) : node.sdo.download(0x2100, 2, b'\x03')
            elif(args.adcRate == 101.1) : node.sdo.download(0x2100, 2, b'\x04')
            elif(args.adcRate == 1.88) : node.sdo.download(0x2100, 2, b'\x05')
            elif(args.adcRate == 3.76) : node.sdo.download(0x2100, 2, b'\x06')
            elif(args.adcRate == 7.51) : node.sdo.download(0x2100, 2, b'\x07')

            print("ADC Conversion rate set to ", args.adcRate)
        
        #adcRange
        if args.adcRange is not None:
            if(args.adcRange == 0.1) : node.sdo.download(0x2100, 3, b'\x00')
            elif(args.adcRange == 0.055) : node.sdo.download(0x2100, 3, b'\x01')
            elif(args.adcRange == 0.025) : node.sdo.download(0x2100, 3, b'\x02')
            elif(args.adcRange == 1) : node.sdo.download(0x2100, 3, b'\x03')
            elif(args.adcRange == 5) : node.sdo.download(0x2100, 3, b'\x04')
            elif(args.adcRange == 2.5) : node.sdo.download(0x2100, 3, b'\x05')
            print("ADC Voltage range set to: ", args.adcRange)
        
        #adcChannels WORKS
        if args.adcChannels is not None:
            node.sdo.download(0x2100, 1, args.adcChannels.to_bytes(1, 'big'))
            print("ADC input channels set to:", args.adcChannels)
            
        #Save to EEPROM
        node.sdo.download(0x1010, 1 , b'\x73\x61\x76\x65')
        print("EEPROM save done")
        
        node.nmt.state = 'OPERATIONAL'